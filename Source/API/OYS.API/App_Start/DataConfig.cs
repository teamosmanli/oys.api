﻿using HDS.App.Api.Decorators;
using OYS.Domain;
using HDS.App.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Tracing;

namespace OYS.API {
    public static class DataConfig {
        public static void Register(HttpConfiguration config) {
            Builder.Bootstrap();
        }
    }
}
