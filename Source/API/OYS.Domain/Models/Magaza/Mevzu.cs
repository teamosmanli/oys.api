﻿using OYS.Domain.Models.Base;
using HDS.App.Api.Membership.Models;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;
using System;
using System.Collections.Generic;

namespace OYS.Domain.Models.Magaza {

    public class Mevzu : OYSBase<Mevzu> {

        public string Tarif { get; set; }

        public override string TextValue => Tarif;

        protected override void EKMap(IDOTableBuilder<Mevzu> builder) {

            builder.MapsTo(x => { x.SchemaName("MZ").TableName("Mevzular"); });

            builder.For(d => d.Tarif).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);

            builder.UniqueKey(d => d.Tarif);
        }

    }

}
