﻿using HDS.App.Domain.Aggregate;
using HDS.App.Domain.Contracts;
using HDS.App.Domain.DML;
using HDS.App.Domain.Enums;
using HDS.App.Domain.Objects;
using HDS.App.Domain.Query;
using HDS.App.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Reflection;

namespace HDS.App.Engines.Core {
    internal class DOEngine<TEntity> : DOSyncEngine<TEntity>, IDOEngine<TEntity>, IGenericEngine
        where TEntity : DOBase<TEntity> {

        public DOEngine(Func<Exception, DMLResponse<TEntity>> fallback)
            : base(fallback) {

        }
        public DOEngine() : base() {

        }
        public async Task<DMLResponse<TEntity>> Delete<TKey>(TKey key) {
            var id = long.Parse(key.ToString());
            DMLResponse<TEntity> response = new DMLResponse<TEntity>() { ContainsItem = false };
            await Q<TEntity>.SelectAllColumns().Where(x => x.ID == id).ExecuteOne(xq => {
                response = xq.Delete();
            }, () => response = new DMLResponse<TEntity>() {
                Fault = new Exception($"Entity ID: {key} not found"),
                ContainsItem = false
            });
            return response;
        }
        public async Task<DMLResponse<TEntity>> Insert(TEntity entity) {
            return await CR.Single(entity).Upsert();
        }
        public async Task<DMLResponse<TEntity>> InsertIfAbsent<TProp>(TEntity entity, Func<TEntity, TProp> discriminator)
            where TProp: class {
            var response = SelectAllSync();
            var discriminated = response.CollectionResponse.SingleOrDefault(e => discriminator(e).Equals(discriminator(entity)));
            if (discriminated == null) {
                return await CR.Single(entity).Upsert();
            }
            return new DMLResponse<TEntity>() { SingleResponse = discriminated ?? entity, ContainsItem = true };
        }
        public async Task<DMLResponse<TEntity>> Update(long id, Func<IUpdateClause<TEntity>, IUpdateClause<TEntity>> updater) {
            var result = new DMLResponse<TEntity>() { ContainsItem = false };
            await Q<TEntity>.SelectAllColumns().Where(u => u.ID == id).ExecuteOne(cursor => {
                result = new DMLResponse<TEntity>(updater(cursor.UpdateClause()).PersistUpdate());
            }, () => result = new DMLResponse<TEntity>() {
                Fault = new Exception($"Entity with ID: {id} not found"),
                ContainsItem = false
            });
            return result;
        }
        public async Task<IDMLResponse> UpdateAggregate(IAGBase aggregate) {
            var result = new DMLResponse<IAGBase>() { ContainsItem = false };
            await Q<TEntity>.SelectAllColumns().Where(u => u.ID == aggregate.IDProperty).ExecuteOne(cursor => {
                result = cursor.UpdateClause().UpsertAggregate(aggregate);
            }, () => result = new DMLResponse<IAGBase>() {
                Fault = new Exception($"Entity with ID: {aggregate.IDProperty} not found"),
                ContainsItem = false
            });
            return result;
        }

        public async Task<DMLResponse<TEntity>> SelectAll(Func<TEntity, bool> filter = null) {
            DMLResponse<TEntity> response = new DMLResponse<TEntity>() { ContainsItem = false };
            var list = new List<TEntity>();
            await Q<TEntity>.SelectAllColumns().ExecuteMany((query) => {
                if (filter != null) {
                    if (filter(query.ResolvedEntity)) list.Add(query.ResolvedEntity);
                } else {
                    list.Add(query.ResolvedEntity);
                }
            });
            response = new DMLResponse<TEntity> { CollectionResponse = list };
            return response;
        }

        public async Task<DMLResponse<TEntity>> SelectBy(params Expression<Func<TEntity, bool>>[] selectors) {
            DMLResponse<TEntity> response = new DMLResponse<TEntity>() { ContainsItem = false };
            var list = new List<TEntity>();
            var confinement = Q<TEntity>.SelectAllColumns().Where(selectors.First());
            selectors.Skip(1).ToList().ForEach(sel => confinement = confinement.And(sel));
            await confinement.ExecuteMany((query) => {
                list.Add(query.ResolvedEntity);
            });
            response = new DMLResponse<TEntity> { CollectionResponse = list };
            return response;
        }

        public async Task<DMLResponse<TEntity>> SelectSingle(Expression<Func<TEntity, bool>> selector) {
            DMLResponse<TEntity> response = new DMLResponse<TEntity>() { ContainsItem = false };
            await Q<TEntity>.SelectAllColumns().Where(selector).ExecuteOne((query) => {
                response = new DMLResponse<TEntity> { SingleResponse = query.ResolvedEntity };
            });
            return response;
        }

        public async Task<DMLResponse<TEntity>> SelectLastRecord<TKey>(Expression<Func<TEntity, TKey>> keySelector) {
            DMLResponse<TEntity> response = new DMLResponse<TEntity>() { ContainsItem = false };
            await Q<TEntity>.SelectAllColumns().Order().By(keySelector, EOrderBy.ASC).ExecuteMany((query) => {
                response = new DMLResponse<TEntity> { SingleResponse = query.ResolvedEntity };
            });
            return response;
        }

        public async Task<DMLResponse<TEntity>> SelectSingleByID<TKey>(TKey key) {
            DMLResponse<TEntity> response = new DMLResponse<TEntity>() { ContainsItem = false };
            await Q<TEntity>.SelectAllColumns().Where(e => (object)e.ID == (object)key).ExecuteOne((query) => {
                response = new DMLResponse<TEntity> { SingleResponse = query.ResolvedEntity };
            });
            return response;
        }

        public async Task<IDMLResponse> GenericSelectAll() {
            return await SelectAll();
        }

        public async Task<IDMLResponse> GenericSelectAll(Func<dynamic, bool> filter = null) {
            return await SelectAll(filter);
        }
        public IDMLResponse GenericSelectAllSync() {
            return SelectAllSync();
        }

        public async Task<IDMLResponse> GenericSelectByID(long id) {
            return await SelectSingleByID(id);
        }

        public IDMLResponse GenericSelectByIDSync(long id) {
            return SelectSingleByIDSync(id);
        }

        public async Task<IDMLResponse> GenericDelete(long id) {
            return await Delete(id);
        }

        public async Task<IDMLResponse> GenericUpdateAggregate(IAGBase aggregate) {
            return await UpdateAggregate(aggregate);
        }

        public IDMLResponse GenericSelectByComparison<T>(PropertyInfo targetProp, T target) {
            return SelectByComparison(targetProp, target);
        }
    }
}
