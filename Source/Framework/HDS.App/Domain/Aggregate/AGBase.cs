﻿
using HDS.App.Domain.Data;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;
using HDS.App.Extensions.Static;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using HDS.App.Domain.Contracts;
using HDS.App.Domain.DML;
using HDS.App.Engines;
using HDS.App.Engines.Core;

namespace HDS.App.Domain.Aggregate {

    public abstract class AGBase<TAggregate> : IAGBase
        where TAggregate : AGBase<TAggregate> {

        public AGBase() {
            _Builder = GetBuilder();
        }

        private IAGViewBuilder<TAggregate> _Builder;
        private IAGViewBuilder<TAggregate> GetBuilder() {
            IAGViewBuilder<TAggregate> builder = new AGViewBuilder<TAggregate>();
            Map(builder);
            return builder;
        }

        protected abstract void Map(IAGViewBuilder<TAggregate> builder);
        public abstract Type MainDomain { get; }
        public abstract Type[] ReferencedDomains { get; }
        public abstract string TextValue { get; }
        public abstract string ResourceName { get; }

        [TableColumn(Order = 0)]
        public long IDProperty {
            get {
                var props = typeof(TAggregate).GetProperties(BindingFlags.Instance | BindingFlags.Public)
                        .Where(p => p.GetCustomAttribute<IDPropertyLocatorAttribute>() != null);
                if (props.Count() > 1) {
                    throw new InvalidOperationException("Multiple IDPropertyLocator Specified");
                } else if (props.Count() == 0) {
                    throw new InvalidOperationException("None of Multiple IDPropertyLocator Specified");
                }
                var prop = props.First();
                return Convert.ToInt64(prop.GetValue(this));
            }
            set {
                var props = typeof(TAggregate).GetProperties(BindingFlags.Instance | BindingFlags.Public)
                                        .Where(p => p.GetCustomAttribute<IDPropertyLocatorAttribute>() != null);
                if (props.Count() > 1) {
                    throw new InvalidOperationException("Multiple IDPropertyLocator Specified");
                } else if (props.Count() == 0) {
                    throw new InvalidOperationException("None of Multiple IDPropertyLocator Specified");
                }
                var prop = props.First();
                prop.SetValue(this, value);
            }
        }

        public TDomain GetMappedDomain<TDomain>()
            where TDomain : DOBase<TDomain> {
            return (_Builder as AGViewBuilder<TAggregate>).GetMappedDomain(typeof(TDomain), this as TAggregate) as TDomain;
        }

        public IDOBase GetMappedDomainByType(Type domainType) {
            if (domainType.IsSubclassOf(typeof(DOBase<>).MakeGenericType(domainType))) {
                return (_Builder as AGViewBuilder<TAggregate>).GetMappedDomain(domainType, this as TAggregate);
            } else {
                throw new ArgumentException($"{domainType.ToString()} Should Have Been Assignable From DOBase<>");
            }
        }
        public bool IsColumnRequired(PropertyInfo property) {
            return (_Builder as AGViewBuilder<TAggregate>).IsColumnRequired(property);
        }
        public long GetMaxLength(MemberInfo key) {
            return (_Builder as AGViewBuilder<TAggregate>).GetMaxLength(key);
        }
        public IAGSchemaBuilder GetSchemaBuilder() {

            return _Builder.SchemaBuilder;

        }

        public IAGBase Drop() {
            using (var engine = SDbParams.DataTools.GenerateEngine()) {
                var commandText = "";
                switch (SDbParams.CurrentServerType) {
                    case Data.EServerType.MySql:
                        commandText = "DROP VIEW IF EXISTS {1}";
                        break;
                    case Data.EServerType.MsSql:
                        commandText = "IF EXISTS(select * FROM sys.views where name = '{0}') DROP VIEW {1}";
                        break;
                    default:
                        throw new ArgumentException("Unsupported Server Type");
                }
                using (var command = engine.ConnectifiedCommand(commandText.Puts(_Builder.SchemaBuilder.GetViewName(),
                        _Builder.SchemaBuilder.GetFormatted()))) {
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
            }
            return this;

        }

        public void BuildView() {
            var viewAndSchema = _Builder.SchemaBuilder.Build();
            var selectStatement = _Builder.QueryString
                .Replace(", #selectors#", "")
                .Replace(" #joinlist#", "");
            var commandText = "";
            switch (SDbParams.CurrentServerType) {
                case Data.EServerType.MySql:
                    commandText = "CREATE VIEW {0} AS {1}".Puts(viewAndSchema, selectStatement);
                    break;
                case Data.EServerType.MsSql:
                    var countStatement = Regex.Replace(selectStatement.ToString(), @"(SELECT )(.*)( FROM.*)", "$1COUNT(*)$3");
                    var createViewStatement = selectStatement.Insert(selectStatement.ToString().ToUpper().IndexOf("SELECT") + 7, "TOP ({0})".Puts(countStatement));
                    commandText = "CREATE VIEW {0} WITH SCHEMABINDING AS {1}".Puts(viewAndSchema, createViewStatement);
                    break;
                default:
                    throw new ArgumentException("Unsupported Server Type");
            }

            using (var engine = SDbParams.DataTools.GenerateEngine()) {
                using (var command = engine.ConnectifiedCommand(commandText)) {
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
            }
        }

        public string Slug {
            get {
                return _Builder.SchemaBuilder.GetViewName().ToLower();
            }
        }

        public override string ToString() {
            return this.TextValue;
        }

        public IDMLResponse InsertSelf(bool commitMainDomain = true) {

            IDMLResponse boundResponse = null;
            var domain = this.GetMappedDomainByType(this.MainDomain);
            var viewBinders = this.ResolveProperties(this.GetType()).Where(p => p.HasAttribute<ViewBinderAttribute>())
                .ToDictionary(prop => prop, prop => prop.GetCustomAttribute<ViewBinderAttribute>());
            var boundIDs = this.ResolveProperties(this.GetType()).Where(p => p.HasAttribute<ViewBinderAttribute>())
                .ToDictionary(b => Activator.CreateInstance(b.GetCustomAttribute<ViewBinderAttribute>().BoundType).As<IAGBase>().MainDomain, b => 0L);
            foreach (var viewBinder in viewBinders) {
                if (viewBinder.Value.BoundType.Equals(this.GetType()) == false) {
                    var id = viewBinder.Key.GetValue(this).As<long>();
                    boundResponse = SDataEngine.GenerateAGEngine(viewBinder.Value.BoundType).SelectByIDSync(id);
                    var boundAggregate = Activator.CreateInstance(viewBinder.Value.BoundType).As<IAGBase>();
                    if (boundResponse.ContainsItem) {
                        var referencedDomains = boundAggregate.ReferencedDomains.ToList();
                        referencedDomains.ForEach(rd => boundIDs[rd] = boundResponse.EntityID);
                    }
                    this.MainDomain.GetProperty(viewBinder.Value.BoundProperty).SetValue(domain, id);
                } else {
                    throw new ArgumentException($"{viewBinder.Value.BoundType} is self refernced from itself with ViewBinder Attribute");
                }
            }

            IDMLResponse domainResp = null;
            if (commitMainDomain) {
                domainResp = domain.InsertSelf();
                if (domainResp.ContainsItem == false) {
                    return InsertSelf(false);
                }
            }
            var referencedIDs = this.ReferencedDomains.ToDictionary(t => t, t => 0L);
            referencedIDs[this.MainDomain] = domain.ID;
            foreach (var rdType in this.ReferencedDomains) {
                if (!rdType.IsSubclassOf(typeof(DOBase<>).MakeGenericType(rdType))) {
                    throw new ArgumentException($"Type: {rdType.ToString()} Not Assignable From DOBase<{rdType.ToString()}>");
                }
                var rdomain = this.GetMappedDomainByType(rdType);
                foreach (var keys in rdomain.GetKeys()) {
                    //key = KunyeID
                    var rt = keys.referencing.ReflectedType.GetGenericArguments()?.FirstOrDefault() ?? keys.referencing.ReflectedType;
                    if (referencedIDs.ContainsKey(rt)) {
                        keys.foreign.SetValue(rdomain, referencedIDs[rt] == 0 ? null : (object)referencedIDs[rt]);
                    } else if (boundIDs.ContainsKey(rt)) {
                        keys.foreign.SetValue(rdomain, boundIDs[rt] == 0 ? null : (object)boundIDs[rt]);
                    }
                }
                var single = typeof(CR).ExecuteStaticMethod("Single", new Type[] { rdType }, rdomain);
                var dmlResponse = single.ExecuteMethod("UpsertSync", Type.EmptyTypes) as IDMLResponse;
                if (dmlResponse.ContainsItem) {
                    domain.GetKeys().SingleOrDefault(p => p.referencing.ReflectedType.GetGenericArguments()?.FirstOrDefault().Equals(rdType) == true)
                        .foreign?.SetValue(domain, dmlResponse.EntityID);
                    referencedIDs[rdType] = dmlResponse.EntityID;
                } else {
                    return dmlResponse;
                }
            }
            if (commitMainDomain == false) {
                domainResp = domain.InsertSelf();
                if (domainResp.ContainsItem == false) {
                    return domainResp;
                }
            }
            return domainResp;

        }

        public IDMLResponse DeleteSelf() {
            //HavingCollection is being deleted
            foreach (var prop in this.ResolveProperties()) {
                if (prop.HasAttribute<HavingCollectionAttribute>(out var attr)) {
                    var collection = prop.GetValue(this);
                    var elementType = collection.GetType().GetGenericArguments().FirstOrDefault();
                    IGenericEngine elementEng = null;
                    if (typeof(IAGBase).IsAssignableFrom(elementType)) {
                        elementEng = Activator.CreateInstance(typeof(AGEngine<>).MakeGenericType(elementType)) as IGenericEngine;
                        var stronglyElement = Activator.CreateInstance(elementType).As<IAGBase>();
                        var targetProp = elementType.GetProperty(attr.InverseProperty);
                        IDMLResponse listedCollectionResponse = elementEng.GenericSelectByComparison(targetProp, this.IDProperty);
                        if (listedCollectionResponse.ContainsItem == true) {
                            foreach (IAGBase item in listedCollectionResponse.DynamicCollectionResponse) {
                                var dmn = item.GetMappedDomainByType(item.MainDomain);
                                var dmnDeleteResp = dmn.DeleteSelf();
                                if (dmnDeleteResp.ContainsItem == false) {
                                    return dmnDeleteResp;
                                }
                            }
                        }
                    } else {
                        elementEng = Activator.CreateInstance(typeof(DOEngine<>).MakeGenericType(elementType)) as IGenericEngine;
                        var stronglyElement = Activator.CreateInstance(elementType).As<IDOBase>();
                        var targetProp = elementType.GetProperty(attr.InverseProperty);
                        IDMLResponse listedCollectionResponse = elementEng.GenericSelectByComparison(targetProp, this.IDProperty);
                        if (listedCollectionResponse.ContainsItem == true) {
                            foreach (IDOBase item in listedCollectionResponse.DynamicCollectionResponse) {
                                var dmnDeleteResp = item.DeleteSelf();
                                if (dmnDeleteResp.ContainsItem == false) {
                                    return dmnDeleteResp;
                                }
                            }
                        }
                    }
                }
            }
            var domain = this.GetMappedDomainByType(this.MainDomain);
            var deleteResponse = domain.DeleteSelf();
            if (deleteResponse.ContainsItem == true) {
                foreach (var rdType in this.ReferencedDomains) {
                    if (!rdType.IsSubclassOf(typeof(DOBase<>).MakeGenericType(rdType))) {
                        throw new ArgumentException($"Type: {rdType.ToString()} Not Assignable From DOBase<{rdType.ToString()}>");
                    }
                    var rdomain = this.GetMappedDomainByType(rdType);
                    var dmlResponse = rdomain.DeleteSelf();
                    if (dmlResponse.ContainsItem == false) {
                        return dmlResponse;
                    }
                }
            }
            return new DMLResponse<dynamic> {
                SingleResponse = this,
                ContainsItem = true
            };
        }

        [JsonIgnore]
        public IEnumerable<(MemberInfo domain, MemberInfo aggragate)> Mappings {
            get {
                return (_Builder as AGViewBuilder<TAggregate>).Mappings.ToArray();
            }
        }

    }

}