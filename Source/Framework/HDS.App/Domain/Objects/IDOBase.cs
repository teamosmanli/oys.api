﻿
using HDS.App.Domain.Aggregate;
using HDS.App.Domain.Contracts;
using HDS.App.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace HDS.App.Domain.Objects {

    public interface IDOBase : IDomainBaseModel {

        void BuildTable();

        IDOBase BuildConstraints();

        int? GetMaxLengthOf(MemberInfo key);

        IEnumerable<MemberInfo> TakeFields(EResolveBy by);

        IEnumerable<DOPropBuilder> TakeBuilders();

        long ID { get; set; }

        IDOBase Drop();

        IDMLResponse InsertSelf();

        IDMLResponse DeleteSelf();

        IDOSchemaBuilder SchemaBuilder { get; }
        
        IEnumerable<MemberInfo> GetRelationKeys();

        IEnumerable<(MemberInfo foreign, MemberInfo referencing)> GetKeys();

    }

}