﻿using HDS.App.Domain.Aggregate;
using HDS.App.Domain.Contracts;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HDS.App.Domain.Objects {

    public class DMLResponse<TEntity> : IDMLResponse
        where TEntity : class {

        private string _Message;


        [JsonIgnore]
        public long EntityID {
            get {
                if (typeof(IDOBase).IsAssignableFrom(DynamicSingleResponse.GetType())) {
                    return (DynamicSingleResponse as IDOBase).ID;
                } else if (typeof(IAGBase).IsAssignableFrom(DynamicSingleResponse.GetType())) {
                    return (DynamicSingleResponse as IAGBase).IDProperty;
                } else {
                    throw new ArgumentException($"EntityID.get called on {DynamicSingleResponse.GetType().ToString()} which should have been of DOBase<> or AGBase<>");
                }
            }
        }
        public Exception Fault { get; set; }

        private bool _ContainsItem { get; set; }
        public bool ContainsItem {
            get => _ContainsItem && Fault == null;
            set => _ContainsItem = value;
        }
        public string Message {
            get {
                return ContainsItem ? _Message : $"{Fault?.Message} > {Fault?.StackTrace}" ?? "";
            }
            set {
                _Message = value;
            }
        }

        public DMLResponse() {
            ContainsItem = true;
        }

        public DMLResponse(DMLResponse<IDOBase> response) {
            this._Message = response._Message;
            this.Fault = response.Fault;
            this.ContainsItem = response.ContainsItem;
            this.singleResponse = response.singleResponse as TEntity;
            this.collectionResponse = response.collectionResponse as IEnumerable<TEntity>;
        }

        private IEnumerable<TEntity> collectionResponse;
        public IEnumerable<TEntity> CollectionResponse {
            get => dynamicCollectionResponse as ICollection<TEntity> ?? collectionResponse;
            set => dynamicCollectionResponse = collectionResponse = value;
        }

        private IEnumerable<dynamic> dynamicCollectionResponse;
        [JsonIgnore]
        public IEnumerable<dynamic> DynamicCollectionResponse {
            get => collectionResponse.Select(cr => (dynamic)cr) ?? dynamicCollectionResponse;
            set => dynamicCollectionResponse = collectionResponse = value.Select(cr => cr as TEntity);
        }

        private TEntity singleResponse;
        public TEntity SingleResponse {
            get => dynamicSingleResponse as TEntity ?? singleResponse;
            set => dynamicSingleResponse = singleResponse = value;
        }

        private dynamic dynamicSingleResponse;

        [JsonIgnore]
        public dynamic DynamicSingleResponse {
            get => (dynamic)singleResponse ?? dynamicSingleResponse;
            set => dynamicSingleResponse = singleResponse = value as TEntity;
        }
    }
}
