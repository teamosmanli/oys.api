﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using HDS.App.Api.Decorators;

namespace HDS.App.Api.Common {
    public class ApiMetadata {
        public ApiConfigAttribute ApiConfig { get; internal set; }
        public IEnumerable<(MethodInfo method, HQRouteAttribute attr)> RouteInfos { get; internal set; }
    }
}
