﻿
using HDS.App.Domain.Aggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HDS.App.Api.Decorators {
    [AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class HQPutAttribute : HQRouteAttribute {
        public HQPutAttribute(string path) : base(path) {
            ArgumentType = typeof(IAGBase);
        }

        public override HttpMethod Method => HttpMethod.Put;
    }
}
