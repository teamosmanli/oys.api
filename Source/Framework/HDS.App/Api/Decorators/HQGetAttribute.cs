﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HDS.App.Api.Decorators {
    [AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class HQGetAttribute : HQRouteAttribute {
        public HQGetAttribute(string path) : base(path) {
        }

        public override HttpMethod Method => HttpMethod.Get;
    }
}
