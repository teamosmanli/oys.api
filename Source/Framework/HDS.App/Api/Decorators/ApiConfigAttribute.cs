﻿using HDS.App.Domain.Aggregate;
using HDS.App.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HDS.App.Api.Decorators {
    [System.AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public sealed class ApiConfigAttribute : Attribute {

        readonly string key;
        readonly Type controller;
        readonly Type domain;
        readonly Type aggregate;
        
        public ApiConfigAttribute(string key, Type controller, Type aggregate) {
            this.key = key;
            this.controller = controller;
            this.domain = Activator.CreateInstance(aggregate).As<IAGBase>().MainDomain;
            this.aggregate = aggregate;
        }

        public string Key => key;
        public Type Controller => controller;
        public Type Domain => domain;
        public Type Aggregate => aggregate;

    }
}
