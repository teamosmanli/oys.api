﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;

namespace HDS.App.Api.Membership.Models {
    public class Vazife : MembershipDOBase<Vazife> {

        public string Tarif { get; set; }

        public override string TextValue => Tarif;

        protected override void EMMap(IDOTableBuilder<Vazife> builder) {
            builder.MapsTo(x => { x.SchemaName("AA").TableName("Vazifeler"); });

            builder.For(d => d.Tarif).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(255);
            
        }
    }
}
