﻿using HDS.App.Api.Membership.Models;
using HDS.App.Domain.Contracts;
using HDS.App.Engines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HDS.App.Api.Membership {
    public static class Celse {

        private static IDOEngine<Aza> azaeng = SDataEngine.GenerateDOEngine<Aza>();

        public static bool Cari(string anahtar, out Aza aza) {
            aza = azaeng.SelectSingleSync(a => a.AccessToken == anahtar).SingleResponse;
            return aza != null;
        }

    }
}