﻿using System;
using HDS.App.Domain.Aggregate;
using HDS.App.Api.Membership.Models;
using HDS.App.Extensions.Decorators;
using HDS.App.Api.Membership.Views.Resources;

namespace HDS.App.Api.Membership.Views {

    [ResourceLocator(typeof(TahditResource))]
    public class TahditView : AGBase<TahditView> {

        [IDPropertyLocator]
        public long TahditID { get; set; }

        public long TahditAzaID { get; set; }

        [TableColumn(Order = 1)]
        public string TahditTarif { get; set; }

        [TableColumn(Order = 2)]
        public string VazifeTarif { get; set; }

        [TableColumn(Order = 3)]
        public string KaideTarif { get; set; }

        [TableColumn(Order = 4)]
        public string KaideUnsur { get; set; }

        public override Type MainDomain => typeof(Tahdit);

        public override Type[] ReferencedDomains => new[] { typeof(Kaide) };

        public override string TextValue => TahditTarif;

        public override string ResourceName => "tahdit";

        protected override void Map(IAGViewBuilder<TahditView> builder) {
            builder
                .MapsTo(x => x.SchemaName("VW").ViewName("TahditView"))
                .Select<Tahdit>(li =>
                    li
                    .Map(x => x.ID, x => x.TahditID)
                    .Map(x => x.Tarif, x => x.TahditTarif)
                ).OuterJoin<Aza>(li => li.Map(x => x.ID, x => x.TahditAzaID))
                .On<Tahdit>((a, b) => a.ID == b.AzaID)
                .OuterJoin<Vazife>(li =>
                    li
                    .Map(x => x.Tarif, x => x.VazifeTarif)
                ).On<Tahdit>((a, b) => a.ID == b.VazifeID)
                .OuterJoin<Kaide>(li =>
                    li
                    .Map(x => x.Unsur, x => x.KaideUnsur)
                    .Map(x => x.Tarif, x => x.KaideTarif)
                ).On<Tahdit>((a, b) => a.ID == b.KaideID);
        }
    }
}